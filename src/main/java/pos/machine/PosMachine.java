package pos.machine;

import java.util.*;

import static pos.machine.ItemsLoader.loadAllItems;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItemList = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItemList);
        String receiptStr = renderReceipt(receipt);
        return receiptStr;
    }

    private String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        String receiptStr = generateReceipt(itemsReceipt, receipt.getTotalPrice());
        return receiptStr;
    }

    private String generateReceipt(String itemsReceipt, Integer totalPrice) {
        String receipt = "***<store earning no money>Receipt***\n" + itemsReceipt;
        receipt = receipt + "----------------------\n"+ "Total: " + totalPrice +" (yuan)\n" + "**********************";
        return receipt;
    }

    private String generateItemsReceipt(Receipt receipt) {
        String itemsReceipt = "";
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        for (ReceiptItem receiptItem : receiptItems) {
            String receiptItemStr = receiptItem.toString();
            itemsReceipt = itemsReceipt + receiptItemStr + "\n";
        }
        return itemsReceipt;
    }

    private Receipt calculateCost(List<ReceiptItem> receiptItemList) {
        calculateItemsCost(receiptItemList);
        int totalPrice = calculateTotalPrice(receiptItemList);
        return new Receipt(receiptItemList, totalPrice);
    }

    private List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItemList) {
        for (ReceiptItem receiptItem : receiptItemList){
            receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
        }
        return receiptItemList;
    }

    private int calculateTotalPrice(List<ReceiptItem> receiptItemList) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItemList) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    private List<ReceiptItem> decodeToItems(List<String> barcodes){
        List<ReceiptItem> receiptItemList = new ArrayList<>();

        Map<String, Integer> barcodeQuantityMap = new TreeMap<>();
        for (String barcode : barcodes) {
            if (barcodeQuantityMap.containsKey(barcode)){
                barcodeQuantityMap.put(barcode, barcodeQuantityMap.get(barcode) + 1);
            }else {
                barcodeQuantityMap.put(barcode, 1);
            }
        }
        for (String barcode : barcodeQuantityMap.keySet()) {
            Item item = findByBarcode(barcode);
            if (item != null){
                int quantity = barcodeQuantityMap.get(barcode);
                int unitPrice = item.getPrice();
                ReceiptItem receiptItem = new ReceiptItem(item.getName(), quantity, unitPrice);
                receiptItemList.add(receiptItem);
            }
        }
        return receiptItemList;
    }

    private Item findByBarcode(String barcode){
        List<Item> items = loadAllItems();
        for (Item item : items) {
            if (item.getBarcode().equals(barcode)){
                return item;
            }
        }
        return null;
    }



}
